//
//  AppsSpecifications.swift
//  Items!Specifications
//
//  Created by Manuel Meyer on 29.10.22.
//

import Quick
import Nimble
import ItemsModels
import ItemsApp
@testable import Items_


typealias AppStore = Store<AppState,AppState.Change>

final class AppsSpecifications: QuickSpec {
    override class func spec() {
        var store      :AppStore!
        var roothandler:((Message) -> ())!
        var app        :Input!
        beforeEach{
            store = createDiskStore(pathInDocs:"appspecs.json")
            roothandler = { msg in }
            app = createAppDomain(store:store, receivers:[], rootHandler:roothandler)
        }
        afterEach{
            destroy(&store)
            roothandler = nil
            app = nil
        }
        
        describe("ItemsApp") {
            context("uninitialized") {
                it("has no todos") { expect(store.state().todos.items).to(haveCount(0)) }
            }
            context("initialized") {
                it("has todos object"     ) { expect(store.state().todos      ).toNot(beNil())   }
                it("has empty todos items") { expect(store.state().todos.items).to   (beEmpty()) }
                context("add") {
                    let i0 = TodoItem(title:"hey Ho")
                    beforeEach {
                        app(.todos(.cmd(.add(item:i0))))
                    }
                    it("has a todo item") { expect(store.state().todos.items).to(equal([i0])) }
                    context("add") {
                        let i1 = TodoItem(title:"Let's go")
                        beforeEach {
                            app(.todos(.cmd(.add(item:i1))))
                        }
                        it("has two todo items") { expect(store.state().todos.items).to(equal([i0,i1])) }
                        context("remove") {
                            beforeEach {
                                app(.todos(.cmd(.remove(item:i0))))
                            }
                            it("has the added item, id") { expect(store.state().todos.items.first?.id).to(equal(i1.id)) }
                            it("has a todo item"       ) { expect(store.state().todos.items          ).to(haveCount(1)) }
                            context("remove twice fails silently") {
                                beforeEach {
                                    app(.todos(.cmd(.remove(item:i0))))
                                }
                                it("has a todo item") { expect(store.state().todos.items.map(\.id)).to(equal([i1.id])) }
                            }
                        }
                    }
                    context("remove") {
                        beforeEach {
                            app(.todos(.cmd(.remove(item:i0))))
                        }
                        it("has todos object"     ) { expect(store.state().todos      ).toNot(beNil()  ) }
                        it("has empty todos items") { expect(store.state().todos.items).to   (beEmpty()) }
                        context("removing unpresent item fails silently") {
                            beforeEach {
                                app(.todos(.cmd(.remove(item:i0))))
                            }
                            it("has todos object"     ) { expect(store.state().todos      ).toNot(beNil()  ) }
                            it("has empty todos items") { expect(store.state().todos.items).to   (beEmpty()) }
                        }
                    }
                    context("update") {
                        beforeEach {
                            app(.todos(.cmd(.update(item: i0.alter(.title(to:"Hey Ho!"))))))
                        }
                        it("has a todo item"                  ) { expect(store.state().todos.items             ).to(haveCount(1)    ) }
                        it("has the added item, title"        ) { expect(store.state().todos.items.first?.title).to(equal("Hey Ho!")) }
                        it("has the added item, id"           ) { expect(store.state().todos.items.first?.id   ).to(equal(i0.id)    ) }
                        it("has the items completed unchanged") { expect(store.state().todos.items.first?.state).to(equal(i0.state) ) }
                        it("no due date known"                ) { expect(store.state().todos.items.first?.due  ).to(equal(i0.due)   ) }
                        context("inexistent item") {
                            beforeEach {
                                app(.todos(.cmd(.update(item:TodoItem(title:"new item")))))
                            }
                            it("will not update anything") { expect(store.state().todos.items.map{$0.title}).toNot(contain("new item")) }
                            it("has still one todo item" ) { expect(store.state().todos.items              ).to   (haveCount(1)       ) }
                        }
                    }
                }
            }
        }
    }
}
