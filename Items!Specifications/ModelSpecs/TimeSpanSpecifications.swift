//
//  TimeSpanSpecifications.swift
//  Items!Specifications
//
//  Created by vikingosegundo on 16/11/2022.
//

import Foundation
import Quick
import Nimble
import ItemsModels

final class TimeSpanSpecifications: QuickSpec {
    override class func spec() {
        context("start/end") {
            context("start") {
                let start:TimeSpan.Start = .from(.tomorrow.noon)
                it("has an id"             ) { expect(start.id  ).toNot(beNil()              ) }
                it("has correct satrt data") { expect(start.date).to   (equal(.tomorrow.noon)) }
            }
            context("end") {
                let end:TimeSpan.End = .to(.tomorrow.dayAfter.noon)
                it("has an id") { expect(end.id).toNot(beNil()) }
            }
            context("timespan") {
                context("start") {
                    let start:TimeSpan.Start = .from(.tomorrow.noon)
                    let ts:TimeSpan = .start(start,.unknown)
                    it("has an id"        ) { expect(ts.id   ).toNot(beNil()              ) }
                    it("has correct start") { expect(ts.start).to   (equal(.tomorrow.noon)) }
                    it("has no end"       ) { expect(ts.end  ).to   (beNil()              ) }
                }
                context("start and end") {
                    let start:TimeSpan.Start = .from(.tomorrow.noon         )
                    let end  :TimeSpan.End   = .to  (.tomorrow.dayAfter.noon)
                    let ts:TimeSpan = .start(start,end)
                    it("has an id"        ) { expect(ts.id   ).toNot(beNil()                       ) }
                    it("has correct start") { expect(ts.start).to   (equal(.tomorrow.noon)         ) }
                    it("has correct end"  ) { expect(ts.end  ).to   (equal(.tomorrow.dayAfter.noon)) }
                }
                context("false start and end") {
                    let start:TimeSpan.Start = .from(.tomorrow.dayAfter.noon)
                    let end  :TimeSpan.End   = .to  (.tomorrow.noon)
                    let ts:TimeSpan = .start(start,end)
                    it("has an id"           ) { expect(ts.id   ).toNot(beNil()                       ) }
                    it("has impossible start") { expect(ts.start).to   (equal(.tomorrow.dayAfter.noon)) }
                    it("has impossible end"  ) { expect(ts.end  ).to   (equal(.tomorrow.noon)         ) }
                }
            }
        }
        context("duration") {
            context("of 2 days") {
                let start:TimeSpan.Start = .from(.tomorrow.noon)
                let timeComp: TimeSpan.TimeComponent = .days
                let ts:TimeSpan = .duration(start,for:2,timeComp)
                it("timecomp has id") { expect(timeComp.id).toNot(beNil()                                ) }
                it("timespan start" ) { expect(ts.start   ).to   (equal(.tomorrow.noon)                  ) }
                it("timespan end"   ) { expect(ts.end     ).to   (equal(.tomorrow.noon.dayAfter.dayAfter)) }
            }
            context("of 2 hours") {
                let start:TimeSpan.Start = .from(.tomorrow.noon)
                let timeComp: TimeSpan.TimeComponent = .hours
                let ts:TimeSpan = .duration(start,for:2,timeComp)
                it("timecomp has id") { expect(timeComp.id).toNot(beNil()                                            ) }
                it("timespan start" ) { expect(ts.start).to(equal(.tomorrow.noon)                                    ) }
                it("timespan end"   ) { expect(ts.end  ).to(equal(cal.date(byAdding:.init(hour:2),to:.tomorrow.noon))) }
            }
            context("of 2 minutes") {
                let start:TimeSpan.Start = .from(.tomorrow.noon)
                let timeComp: TimeSpan.TimeComponent = .minutes
                let ts:TimeSpan = .duration(start,for:2,timeComp)
                it("timecomp has id") { expect(timeComp.id).toNot(beNil()                                              ) }
                it("timespan start" ) { expect(ts.start).to(equal(.tomorrow.noon)                                      ) }
                it("timespan end"   ) { expect(ts.end  ).to(equal(cal.date(byAdding:.init(minute:2),to:.tomorrow.noon))) }
            }
            context("of 2 seconds") {
                let start:TimeSpan.Start = .from(.tomorrow.noon)
                let timeComp: TimeSpan.TimeComponent = .seconds
                let ts:TimeSpan = .duration(start,for:2,timeComp)
                it("timecomp has id") { expect(timeComp.id).toNot(beNil()                                              ) }
                it("timespan start" ) { expect(ts.start).to(equal(.tomorrow.noon)                                      ) }
                it("timespan end"   ) { expect(ts.end  ).to(equal(cal.date(byAdding:.init(second:2),to:.tomorrow.noon))) }
            }
        }
    }
}

fileprivate let cal = Calendar.autoupdatingCurrent
