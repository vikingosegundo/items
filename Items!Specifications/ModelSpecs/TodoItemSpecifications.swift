//
//  TodoItemSpecifications.swift
//  Items!Specifications
//
//  Created by Manuel Meyer on 30.10.22.
//

import Quick
import Nimble
import ItemsModels

final class TodoItemSpecifications: QuickSpec {
    override  class func spec() {
        describe("TodoItem") {
            let t0 = TodoItem(title:"Get Coffee")
            context("just created") {
                it("has an id"          ) { expect(t0.id      ).toNot(beNil()            ) }
                it("has provided title" ) { expect(t0.title   ).to   (equal("Get Coffee")) }
                it("has empty details"  ) { expect(t0.details ).to   (equal("")          ) }
                it("has unkown due date") { expect(t0.due     ).to   (equal(.unknown)    ) }
                it("isnt finished"      ) { expect(t0.state   ).to   (equal(.unfinished) ) }
                it("has no location"    ) { expect(t0.location).to   (equal(.unknown)    ) }
            }
            context("change title") {
                let t1 = t0.alter(.title(to:"Get Coffee — ASAP"))
                it("has same id"          ) { expect(t1.id      ).to   (equal(t0.id)              ) }
                it("has different title"  ) { expect(t1.title   ).toNot(equal(t0.title)           ) }
                it("has provided title"   ) { expect(t1.title   ).to   (equal("Get Coffee — ASAP")) }
                it("has unchanged details") { expect(t1.details ).to   (equal(t0.details)         ) }
                it("has same due date"    ) { expect(t1.due     ).to   (equal(t0.due)             ) }
                it("has same state"       ) { expect(t1.state   ).to   (equal(t0.state)           ) }
                it("has same location"    ) { expect(t1.location).to   (equal(t0.location)        ) }
            }
            context("change details") {
                let t1 = t0.alter(.details(to:"FAST"))
                it("has same id"        ) { expect(t1.id      ).to   (equal(t0.id)      ) }
                it("has changed details") { expect(t1.details ).toNot(equal(t0.details) ) }
                it("has correct details") { expect(t1.details ).to   (equal("FAST")     ) }
                it("has different title") { expect(t1.title   ).to   (equal(t0.title)   ) }
                it("has same due date"  ) { expect(t1.due     ).to   (equal(t0.due)     ) }
                it("has same state"     ) { expect(t1.state   ).to   (equal(t0.state)   ) }
                it("has same location"  ) { expect(t1.location).to   (equal(t0.location)) }
            }
            context("change state") {
                let t1 = t0.alter(.finish)
                it("has same id"         ) { expect(t1.id      ).to   (equal(t0.id)      ) }
                it("has different state" ) { expect(t1.state   ).toNot(equal(t0.state)   ) }
                it("has correct state"   ) { expect(t1.state   ).to   (equal(.finished)  ) }
                it("has same title"      ) { expect(t1.title   ).to   (equal(t0.title)   ) }
                it("has same details"    ) { expect(t1.details ).to   (equal(t0.details) ) }
                it("has same due date"   ) { expect(t1.due     ).to   (equal(t0.due)     ) }
                it("has same location"   ) { expect(t1.location).to   (equal(t0.location)) }
                context("change state back") {
                    let t2 = t1.alter(.unfinish)
                    it("has same id"        ) { expect(t2.id      ).to   (equal(t1.id)      ) }
                    it("has different state") { expect(t2.state   ).toNot(equal(t1.state)   ) }
                    it("has correct state"  ) { expect(t2.state   ).to   (equal(.unfinished)) }
                    it("has same title"     ) { expect(t2.title   ).to   (equal(t1.title)   ) }
                    it("has same details"   ) { expect(t2.details ).to   (equal(t2.details) ) }
                    it("has same due date"  ) { expect(t2.due     ).to   (equal(t1.due)     ) }
                    it("has same location"  ) { expect(t2.location).to   (equal(t1.location)) }
                }
            }
            context("change due date") {
                context("to date") {
                    let t1 = t0.alter(.due(to:.date(.tomorrow.noon)))
                    it("has same id"               ) { expect(t1.id      ).to   (equal(t0.id)         ) }
                    it("has different due date"    ) { expect(t1.due     ).toNot(equal(t0.due)        ) }
                    it("has due date tomorrow noon") { expect(t1.due.date).to   (equal(.tomorrow.noon)) }
                    it("has same state"            ) { expect(t1.state   ).to   (equal(t0.state)      ) }
                    it("has correct state"         ) { expect(t1.state   ).to   (equal(.unfinished)   ) }
                    it("has same title"            ) { expect(t1.title   ).to   (equal(t0.title)      ) }
                    it("has same details"          ) { expect(t1.details ).to   (equal(t0.details)    ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id     ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due    ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due    ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state  ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state  ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title  ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details).to   (equal(t1.details) ) }
                    }
                }
                context("to timespan with duration of 3 days") {
                    let t1 = t0.alter(.due(to:.timeSpan(.duration(.from(.tomorrow.noon),for:3,.days))))
                    it("has same id"                    ) { expect(t1.id                 ).to   (equal(t0.id)                                       ) }
                    it("has different due date"         ) { expect(t1.due                ).toNot(equal(t0.due)                                      ) }
                    it("has no due date"                ) { expect(t1.due.date           ).to   (beNil()                                            ) }
                    it("has due date duration time span") { expect(t1.due.timeSpan       ).to   (equal(.duration(.from(.tomorrow.noon),for:3,.days))) }
                    it("has same state"                 ) { expect(t1.state              ).to   (equal(t0.state)                                    ) }
                    it("has correct state"              ) { expect(t1.state              ).to   (equal(.unfinished)                                 ) }
                    it("has same title"                 ) { expect(t1.title              ).to   (equal(t0.title)                                    ) }
                    it("has same details"               ) { expect(t1.details            ).to   (equal(t0.details)                                  ) }
                    it("has timeSpan id"                ) { expect(t1.due.timeSpan?.id   ).toNot(beNil()                                            ) }
                    it("has timespan start"             ) { expect(t1.due.timeSpan?.start).to   (equal(.tomorrow.noon)                              ) }
                    it("has timespan end"               ) { expect(t1.due.timeSpan?.end  ).to   (equal(.tomorrow.dayAfter.dayAfter.dayAfter.noon)   ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id          ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due         ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due         ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state       ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state       ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title       ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details     ).to   (equal(t1.details) ) }
                        it("has no timespan"           ) { expect(t2.due.timeSpan).to   (beNil()           ) }
                    }
                }
                context("to timespan with duration of -3 days fails silenty") {
                    let t1 = t0.alter(.due(to:.timeSpan(.duration(.from(.tomorrow.noon),for:-3,.days))))
                    it("has same id"      ) { expect(t1.id     ).to(equal(t0.id)      ) }
                    it("has same due date") { expect(t1.due    ).to(equal(t0.due)     ) }
                    it("has same state"   ) { expect(t1.state  ).to(equal(t0.state)   ) }
                    it("has correct state") { expect(t1.state  ).to(equal(.unfinished)) }
                    it("has same title"   ) { expect(t1.title  ).to(equal(t0.title)   ) }
                    it("has same details" ) { expect(t1.details).to(equal(t0.details) ) }
                }
                context("to timespan with duration of 3 hours") {
                    let t1 = t0.alter(.due(to:.timeSpan(.duration(.from(.tomorrow.noon),for:3,.hours))))
                    it("has same id"                    ) { expect(t1.id                 ).to   (equal(t0.id)                                             ) }
                    it("has different due date"         ) { expect(t1.due                ).toNot(equal(t0.due)                                            ) }
                    it("has due date duration time span") { expect(t1.due.timeSpan       ).to   (equal(.duration(.from(.tomorrow.noon),for:3,.hours))     ) }
                    it("has same state"                 ) { expect(t1.state              ).to   (equal(t0.state)                                          ) }
                    it("has correct state"              ) { expect(t1.state              ).to   (equal(.unfinished)                                       ) }
                    it("has same title"                 ) { expect(t1.title              ).to   (equal(t0.title)                                          ) }
                    it("has same details"               ) { expect(t1.details            ).to   (equal(t0.details)                                        ) }
                    it("has timeSpan id"                ) { expect(t1.due.timeSpan?.id   ).toNot(beNil()                                                  ) }
                    it("has timespan start"             ) { expect(t1.due.timeSpan?.start).to   (equal(.tomorrow.noon)                                    ) }
                    it("has timespan end"               ) { expect(t1.due.timeSpan?.end  ).to   (equal(cal.date(byAdding:.init(hour:3),to:.tomorrow.noon))) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id           ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due          ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due          ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state        ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state        ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title        ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details      ).to   (equal(t1.details) ) }
                        it("has no timespan"           ) { expect(t2.due.timeSpan ).to   (beNil()           ) }
                    }
                }
                context("to timespan with duration of 3 minutes") {
                    let t1 = t0.alter(.due(to:.timeSpan(.duration(.from(.tomorrow.noon),for:3,.minutes))))
                    it("has same id"                    ) { expect(t1.id                 ).to   (equal(t0.id)                                               ) }
                    it("has different due date"         ) { expect(t1.due                ).toNot(equal(t0.due)                                              ) }
                    it("has due date duration time span") { expect(t1.due.timeSpan       ).to   (equal(.duration(.from(.tomorrow.noon),for:3,.minutes))     ) }
                    it("has same state"                 ) { expect(t1.state              ).to   (equal(t0.state)                                            ) }
                    it("has correct state"              ) { expect(t1.state              ).to   (equal(.unfinished)                                         ) }
                    it("has same title"                 ) { expect(t1.title              ).to   (equal(t0.title)                                            ) }
                    it("has same details"               ) { expect(t1.details            ).to   (equal(t0.details)                                          ) }
                    it("has timeSpan id"                ) { expect(t1.due.timeSpan?.id   ).toNot(beNil()                                                    ) }
                    it("has timespan start"             ) { expect(t1.due.timeSpan?.start).to   (equal(.tomorrow.noon)                                      ) }
                    it("has timespan end"               ) { expect(t1.due.timeSpan?.end  ).to   (equal(cal.date(byAdding:.init(minute:3),to:.tomorrow.noon))) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id          ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due         ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due         ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state       ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state       ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title       ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details     ).to   (equal(t1.details) ) }
                        it("has no timespan"           ) { expect(t2.due.timeSpan).to   (beNil()           ) }
                    }
                }
                context("to timespan with duration of 3 seconds") {
                    let t1 = t0.alter(.due(to:.timeSpan(.duration(.from(.tomorrow.noon),for:3,.seconds))))
                    it("has same id"                    ) { expect(t1.id                 ).to   (equal(t0.id)                                               ) }
                    it("has different due date"         ) { expect(t1.due                ).toNot(equal(t0.due)                                              ) }
                    it("has due date duration time span") { expect(t1.due.timeSpan       ).to   (equal(.duration(.from(.tomorrow.noon),for:3,.seconds))     ) }
                    it("has same state"                 ) { expect(t1.state              ).to   (equal(t0.state)                                            ) }
                    it("has correct state"              ) { expect(t1.state              ).to   (equal(.unfinished)                                         ) }
                    it("has same title"                 ) { expect(t1.title              ).to   (equal(t0.title)                                            ) }
                    it("has same details"               ) { expect(t1.details            ).to   (equal(t0.details)                                          ) }
                    it("has timeSpan id"                ) { expect(t1.due.timeSpan?.id   ).toNot(beNil()                                                    ) }
                    it("has timespan start"             ) { expect(t1.due.timeSpan?.start).to   (equal(.tomorrow.noon)                                      ) }
                    it("has timespan end"               ) { expect(t1.due.timeSpan?.end  ).to   (equal(cal.date(byAdding:.init(second:3),to:.tomorrow.noon))) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id          ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due         ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due         ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state       ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state       ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title       ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details     ).to   (equal(t1.details) ) }
                        it("has no timespan"           ) { expect(t2.due.timeSpan).to   (beNil()           ) }
                    }
                }
                context("to timespan with start/end") {
                    let t1 = t0.alter(.due(to:.timeSpan(.start(.from(.tomorrow.noon),.to(.tomorrow.noon.dayAfter)))))
                    it("has same id"                     ) { expect(t1.id          ).to   (equal(t0.id)                                                     ) }
                    it("has different due date"          ) { expect(t1.due         ).toNot(equal(t0.due)                                                    ) }
                    it("has due date start/end time span") { expect(t1.due.timeSpan).to   (equal(.start(.from(.tomorrow.noon),.to(.tomorrow.noon.dayAfter)))) }
                    it("has same state"                  ) { expect(t1.state       ).to   (equal(t0.state)                                                  ) }
                    it("has correct state"               ) { expect(t1.state       ).to   (equal(.unfinished)                                               ) }
                    it("has same title"                  ) { expect(t1.title       ).to   (equal(t0.title)                                                  ) }
                    it("has same details"                ) { expect(t1.details     ).to   (equal(t0.details)                                                ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id     ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due    ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due    ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state  ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state  ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title  ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details).to   (equal(t1.details) ) }
                    }
                }
                context("to timespan with start no end") {
                    let t1 = t0.alter(.due(to:.timeSpan(.start(.from(.tomorrow.noon),.unknown))))
                    it("has same id"                        ) { expect(t1.id          ).to   (equal(t0.id)                                 ) }
                    it("has different due date"             ) { expect(t1.due         ).toNot(equal(t0.due)                                ) }
                    it("has due date start no end time span") { expect(t1.due.timeSpan).to   (equal(.start(.from(.tomorrow.noon),.unknown))) }
                    it("has same state"                     ) { expect(t1.state       ).to   (equal(t0.state)                              ) }
                    it("has correct state"                  ) { expect(t1.state       ).to   (equal(.unfinished)                           ) }
                    it("has same title"                     ) { expect(t1.title       ).to   (equal(t0.title)                              ) }
                    it("has same details"                   ) { expect(t1.details     ).to   (equal(t0.details)                            ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.due(to:.unknown))
                        it("has same id"               ) { expect(t2.id         ).to   (equal(t1.id)      ) }
                        it("has unkown due date"       ) { expect(t2.due        ).toNot(equal(t1.due)     ) }
                        it("has due date tomorrow noon") { expect(t2.due        ).to   (equal(.unknown)   ) }
                        it("has same state"            ) { expect(t2.state      ).to   (equal(t1.state)   ) }
                        it("has correct state"         ) { expect(t2.state      ).to   (equal(.unfinished)) }
                        it("has same title"            ) { expect(t2.title      ).to   (equal(t1.title)   ) }
                        it("has same details"          ) { expect(t2.details    ).to   (equal(t1.details) ) }
                    }
                }
                context("to timespan with start/end fails silently for mixed up dates") {
                    let t1 = t0.alter(.due(to:.timeSpan(.start(.from(.tomorrow.noon.dayAfter),.to(.tomorrow.noon)))))
                    it("has same id"               ) { expect(t1.id          ).to(equal(t0.id)      ) }
                    it("has unchanged due date"    ) { expect(t1.due         ).to(equal(t0.due)     ) }
                    it("doesnt correct mixed dates") { expect(t1.due.timeSpan).to(beNil()           ) }
                    it("doesnt accept mixed dates" ) { expect(t1.due.timeSpan).to(beNil()           ) }
                    it("has same state"            ) { expect(t1.state       ).to(equal(t0.state)   ) }
                    it("has correct state"         ) { expect(t1.state       ).to(equal(.unfinished)) }
                    it("has same title"            ) { expect(t1.title       ).to(equal(t0.title)   ) }
                    it("has same details"          ) { expect(t1.details     ).to(equal(t0.details) ) }
                    it("has same location"         ) { expect(t1.location    ).to(equal(t0.location)) }
                    }
            }
            context("location") {
                context("Address") {
                    let a = Address(street: "Boterdiep 71-2", city: "Groningen", country: "The Netherlands", zipCode: "9876EG")
                    let t1 = t0.alter(.location(to:.address(a)))
                    it("has same id"           ) { expect(t1.id                 ).to   (equal(t0.id)      ) }
                    it("has different location") { expect(t1.location           ).toNot(equal(t0.location)) }
                    it("has address location"  ) { expect(t1.location.address   ).to   (equal(a)          ) }
                    it("has not coord location") { expect(t1.location.coordinate).to   (beNil()           ) }
                    it("has no directions"     ) { expect(t1.location.directions).to   (beNil()           ) }
                    it("has same title"        ) { expect(t1.title              ).to   (equal(t0.title)   ) }
                    it("has same details"      ) { expect(t1.details            ).to   (equal(t0.details) ) }
                    it("has same due date"     ) { expect(t1.due                ).to   (equal(t0.due)     ) }
                    it("has same state"        ) { expect(t1.state              ).to   (equal(t0.state)   ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.location(to: .unknown))
                        it("has same id"           ) { expect(t2.id                 ).to   (equal(t1.id)      ) }
                        it("has different location") { expect(t2.location           ).toNot(equal(t1.location)) }
                        it("has unknown location"  ) { expect(t2.location           ).to   (equal(.unknown)   ) }
                        it("has coord location"    ) { expect(t2.location.coordinate).to   (beNil()           ) }
                        it("has no addr location"  ) { expect(t2.location.address   ).to   (beNil()           ) }
                        it("has no directions"     ) { expect(t2.location.directions).to   (beNil()           ) }
                        it("has same title"        ) { expect(t2.title              ).to   (equal(t1.title)   ) }
                        it("has same details"      ) { expect(t2.details            ).to   (equal(t1.details) ) }
                        it("has same due date"     ) { expect(t2.due                ).to   (equal(t1.due)     ) }
                        it("has same state"        ) { expect(t2.state              ).to   (equal(t1.state)   ) }
                    }
                }
                context("Coordinate") {
                    let c = Coordinate(latitude:53.3174759,longitude:6.4681397)
                    let t1 = t0.alter(.location(to:.coordinate(c)))
                    it("has same id"           ) { expect(t1.id                 ).to   (equal(t0.id)      ) }
                    it("has different location") { expect(t1.location           ).toNot(equal(t0.location)) }
                    it("has coord location"    ) { expect(t1.location.coordinate).to   (equal(c)          ) }
                    it("has no addr location"  ) { expect(t1.location.address   ).to   (beNil()           ) }
                    it("has no directions"     ) { expect(t1.location.directions).to   (beNil()           ) }
                    it("has same title"        ) { expect(t1.title              ).to   (equal(t0.title)   ) }
                    it("has same details"      ) { expect(t1.details            ).to   (equal(t0.details) ) }
                    it("has same due date"     ) { expect(t1.due                ).to   (equal(t0.due)     ) }
                    it("has same state"        ) { expect(t1.state              ).to   (equal(t0.state)   ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.location(to: .unknown))
                        it("has same id"           ) { expect(t2.id                 ).to   (equal(t1.id)      ) }
                        it("has different location") { expect(t2.location           ).toNot(equal(t1.location)) }
                        it("has unknown location"  ) { expect(t2.location           ).to   (equal(.unknown)   ) }
                        it("has coord location"    ) { expect(t2.location.coordinate).to   (beNil()           ) }
                        it("has no addr location"  ) { expect(t2.location.address   ).to   (beNil()           ) }
                        it("has no directions"     ) { expect(t2.location.directions).to   (beNil()           ) }
                        it("has same title"        ) { expect(t2.title              ).to   (equal(t1.title)   ) }
                        it("has same details"      ) { expect(t2.details            ).to(equal(t1.details)    ) }
                        it("has same due date"     ) { expect(t2.due                ).to   (equal(t1.due)     ) }
                        it("has same state"        ) { expect(t2.state              ).to   (equal(t1.state)   ) }
                    }
                }
                context("Directions") {
                    let d = Directions().alter(.replace(.steps([
                        .step("Use the side entrance to the garden"),
                        .step("back door at the right side"        ),
                        .step("keys are under door mat"            ),
                        .step("right appartment on 3rd floor"      )])))
                    let t1 = t0.alter(.location(to:.directions(d)))
                    it("has same id"                ) { expect(t1.id                           ).to   (equal(t0.id)                                        ) }
                    it("has different location"     ) { expect(t1.location                     ).toNot(equal(t0.location)                                  ) }
                    it("has directions location"    ) { expect(t1.location.directions          ).to   (equal(d)                                            ) }
                    it("has directions with 4 steps") { expect(t1.location.directions?.steps   ).to   (haveCount(4)                                        ) }
                    it("has directions 1 step"      ) { expect(t1.location.directions?.steps[0]).to   (equal(.step("Use the side entrance to the garden" ))) }
                    it("has directions 2 step"      ) { expect(t1.location.directions?.steps[1]).to   (equal(.step("back door at the right side"         ))) }
                    it("has directions 3 step"      ) { expect(t1.location.directions?.steps[2]).to   (equal(.step("keys are under door mat"             ))) }
                    it("has directions 4 step"      ) { expect(t1.location.directions?.steps[3]).to   (equal(.step("right appartment on 3rd floor"       ))) }
                    it("has no addr location"       ) { expect(t1.location.address             ).to   (beNil()                                             ) }
                    it("has same title"             ) { expect(t1.title                        ).to   (equal(t0.title)                                     ) }
                    it("has same details"           ) { expect(t1.details                      ).to   (equal(t0.details)                                   ) }
                    it("has same due date"          ) { expect(t1.due                          ).to   (equal(t0.due)                                       ) }
                    it("has same state"             ) { expect(t1.state                        ).to   (equal(t0.state)                                     ) }
                    context("back to unknown") {
                        let t2 = t1.alter(.location(to: .unknown))
                        it("has same id"           ) { expect(t2.id                 ).to   (equal(t1.id)      ) }
                        it("has different location") { expect(t2.location           ).toNot(equal(t1.location)) }
                        it("has unknown location"  ) { expect(t2.location           ).to   (equal(.unknown)   ) }
                        it("has coord location"    ) { expect(t2.location.coordinate).to   (beNil()           ) }
                        it("has no addr location"  ) { expect(t2.location.address   ).to   (beNil()           ) }
                        it("has no directions"     ) { expect(t2.location.directions).to   (beNil()           ) }
                        it("has same title"        ) { expect(t2.title              ).to   (equal(t1.title)   ) }
                        it("has same details"      ) { expect(t2.details            ).to   (equal(t1.details) ) }
                        it("has same due date"     ) { expect(t2.due                ).to   (equal(t1.due)     ) }
                        it("has same state"        ) { expect(t2.state              ).to   (equal(t1.state)   ) }
                    }
                }
            }
        }
    }
}
fileprivate let cal = Calendar.autoupdatingCurrent
