//
//  StoreSpecifications.swift
//  Items!Specifications
//
//  Created by Manuel Meyer on 30.10.22.
//

import Quick
import Nimble
import ItemsModels
import ItemsApp
@testable import Items_

final class StoreSpecifications:QuickSpec {
    override class func spec() {
        var store:AppStore!;beforeEach{store=createDiskStore(pathInDocs:"storespecs.json")};afterEach{destroy(&store)}
        var updtd:Bool!    ;beforeEach{updtd=false;store?.updated{updtd=true}             };afterEach{updtd=nil      }
        describe("store") {
            context("start default"){
                it("has no todos") { expect(store.state().todos.items).to(beEmpty()) }
            }
            context("setting todos") {
                beforeEach { store.change(.setting(.todos(.init()))) }
                it("has todos"  ) { expect(store.state().todos).toNot(beNil()) }
                it("was updated") { expect(updtd).to(beTrue()                ) }
            }
            context("add items"){
                let i0 = TodoItem(title: "item 0")
                let i1 = TodoItem(title: "item 1")
                beforeEach {
                    store.change(.setting(.todos(.init())))
                    store.change(.add(i0),.add(i1))
                }
                it("has items") { expect(store.state().todos.items).to(haveCount(2)) }
                context("remove item") {
                    beforeEach { store.change(.remove(i1)) }
                    it("has one item") { expect(store.state().todos.items.map(\.id)).to(equal([i0.id])) }
                    it("was updated" ) { expect(updtd).to(beTrue()                                    ) }
                }
                context("reset") {
                    beforeEach { store.reset() }
                    it("has no item") { expect(store.state().todos.items).to(beEmpty()) }
                    it("was updated") { expect(updtd)                    .to(beTrue() ) }
                }
            }
        }
    }
}
