//
//  LocationsSpecifications.swift
//  Items!Specifications
//
//  Created by Manuel Meyer on 30.10.22.
//

import Quick
import Nimble
import ItemsModels
import ItemsApp
@testable import Items_

final class LocationsSpecifications: QuickSpec {
    override class func spec() {
        describe("Location") {
            context("address") {
                let a0 = Address(street:"s",city:"c",country:"c",zipCode:"1")
                let l0 = Location.address(a0)
                it("freshly created"                       ) { expect(l0.address   ).to(equal(a0)) }
                it("inst accessible by location directions") { expect(l0.directions).to(beNil()  ) }
                it("isnt accessible by location coordinate") { expect(l0.coordinate).to(beNil()  ) }
                context("change street") {
                    let a1 = a0.alter(.street("s1"))
                    let l1:Location = .address(a1)
                    it("has another loc id"   ) { expect(l1.id              ).toNot(equal(l0.id)     ) }
                    it("has unchanged addr id") { expect(l1.address?.id     ).to   (equal(a0.id)     ) }
                    it("has changed street"   ) { expect(l1.address?.street ).toNot(equal(a0.street) ) }
                    it("has new street"       ) { expect(l1.address?.street ).to   (equal("s1")      ) }
                    it("has unchanged city"   ) { expect(l1.address?.city   ).to   (equal(a0.city)   ) }
                    it("has unchanged country") { expect(l1.address?.country).to   (equal(a0.country)) }
                    it("has unchanged zipcode") { expect(l1.address?.zipCode).to   (equal(a0.zipCode)) }
                }
                context("change city") {
                    let a1 = a0.alter(.city("c1"))
                    let l1:Location = .address(a1)
                    it("has unchanged id"     ) { expect(l1.address?.id     ).to   (equal(a0.id)     ) }
                    it("has changed city"     ) { expect(l1.address?.city   ).toNot(equal(a0.city)   ) }
                    it("has changed city"     ) { expect(l1.address?.city   ).to   (equal("c1")      ) }
                    it("has unchanged street" ) { expect(l1.address?.street ).to   (equal(a0.street) ) }
                    it("has unchanged country") { expect(l1.address?.country).to   (equal(a0.country)) }
                    it("has unchanged zipcode") { expect(l1.address?.zipCode).to   (equal(a0.zipCode)) }
                }
                context("change country") {
                    let a1 = a0.alter(.country("c1"))
                    let l1:Location = .address(a1)
                    it("has unchanged id"     ) { expect(l1.address?.id     ).to   (equal(a0.id)     ) }
                    it("has changed country"  ) { expect(l1.address?.country).toNot(equal(a0.country)) }
                    it("has changed country"  ) { expect(l1.address?.country).to   (equal("c1")      ) }
                    it("has unchanged city"   ) { expect(l1.address?.city   ).to   (equal(a0.city)   ) }
                    it("has unchanged street" ) { expect(l1.address?.street ).to   (equal(a0.street) ) }
                    it("has unchanged zipcode") { expect(l1.address?.zipCode).to   (equal(a0.zipCode)) }
                }
                context("change zipCode") {
                    let a1 = a0.alter(.zipCode("2"))
                    let l1:Location = .address(a1)
                    it("has unchanged id"     ) { expect(l1.address?.id     ).to   (equal(a0.id)     ) }
                    it("has changed zipcode"  ) { expect(l1.address?.zipCode).toNot(equal(a0.zipCode)) }
                    it("has changed zipcode"  ) { expect(l1.address?.zipCode).to   (equal("2")       ) }
                    it("has unchanged country") { expect(l1.address?.country).to   (equal(a0.country)) }
                    it("has unchanged city"   ) { expect(l1.address?.city   ).to   (equal(a0.city)   ) }
                    it("has unchanged street" ) { expect(l1.address?.street ).to   (equal(a0.street) ) }
                }
            }
            context("coordinate") {
                let c0 = Coordinate(latitude:53.92681,longitude:9.09762)
                let l0:Location = .coordinate(c0)
                it("has coord id"                          ) { expect(l0.coordinate?.id       ).toNot(beNil()        ) }
                it("has loc id"                            ) { expect(l0.id                   ).toNot(beNil()        ) }
                it("freshly created"                       ) { expect(l0.coordinate?.latitude ).to   (equal(53.92681)) }
                it("freshly created"                       ) { expect(l0.coordinate?.longitude).to   (equal( 9.09762)) }
                it("ints accessible by location directions") { expect(l0.directions           ).to   (beNil()        ) }
                it("isnt accessible by location address"   ) { expect(l0.address              ).to   (beNil()        ) }
                it("is accessible by location coordinate"  ) { expect(l0.coordinate           ).to   (equal(c0)      ) }
            }
            context("directions") {
                let d0 = Directions()
                let d1 = d0.alter(.replace(.steps([
                    .step("straight"),
                    .step("left"),
                    .step("up")
                ])))
                let l = Location.directions(d1)
                it("has an id"                             ) { expect(d0.id                  ).toNot(beNil()                        ) }
                it("has an id"                             ) { expect(d0.steps               ).to   (beEmpty()                      ) }
                it("has same id"                           ) { expect(d1.id                  ).to   (equal(d0.id)                   ) }
                it("contains all steps"                    ) { expect(d1.steps.map{$0.text}  ).to   (equal(["straight","left","up"])) }
                it("all steps have unique ids"             ) { expect(Set(d1.steps.map(\.id))).to   (haveCount(3)                   ) }
                it("is accessible by location directions"  ) { expect(l.directions           ).to   (equal(d1)                      ) }
                it("isnt accessible by location address"   ) { expect(l.address              ).to   (beNil()                        ) }
                it("isnt accessible by location coordinate") { expect(l.coordinate           ).to   (beNil()                        ) }
            }
        }
    }
}
