//
//  ItemRemoverSpecifications.swift
//  Items!Specifications
//
//  Created by vikingosegundo on 27.05.24.
//

import Quick
import Nimble
import ItemsApp
import ItemsModels

final class ItemRemoverSpecifications:QuickSpec {
    override class func spec() {
        var itemRemover:ItemRemover!
        var store:AppStore!
        var fetchedItem:TodoItem!
        var item:TodoItem!

        describe("ItemRemover"){
            beforeEach {
                item = TodoItem(title: "Get Coffee")
                store = createDiskStore(pathInDocs: "ItemRemoverSpecifications.json")
                itemRemover = ItemRemover(store: store, responder: { response in
                    switch response {
                    case let .removed(t): fetchedItem = t
                    }
                })
                store.change(.add(item))    
            }
            afterEach {
                destroy(&store)
                itemRemover = nil
                fetchedItem = nil
                item = nil
            }
            context("store") {
                it("has one item") {
                    expect(store.state().todos.items).to(equal([item]))
                }
            }
            
            context("removing Item") {
                beforeEach {
                    itemRemover.request(to: .remove(item))
                }
                it("has no item") {
                    expect(store.state().todos.items).to(beEmpty())
                }
                it("emits response") {
                    expect(fetchedItem).to(equal(item))
                }
            }
        }
    }
}
