//
//  ItemAdderSpecifications.swift
//  Items!Specifications
//
//  Created by vikingosegundo on 27.05.24.
//

import Quick
import Nimble
import ItemsApp
import ItemsModels

final class ItemAdderSpecifications:QuickSpec {
    override class func spec() {
        var itemAdder:ItemAdder!
        var store:AppStore!
        var fetchedItem:TodoItem!
        
        describe("ItemAdder"){
            beforeEach {
                store = createDiskStore(pathInDocs: "ItemAdderSpecifications.json")
                itemAdder = ItemAdder(store: store, responder: { response in
                    switch response {
                    case let .added(t): fetchedItem = t
                    }
                })
            }
            afterEach {
                destroy(&store)
                itemAdder = nil
                fetchedItem = nil
            }
            context("store") {
                it("has no item") {
                    expect(store.state().todos.items).to(beEmpty())
                }
            }
            context("adding Item") {
                var item:TodoItem!
                beforeEach {
                    item = TodoItem(title: "Get Coffee")
                    itemAdder.request(to: .add(item))
                }
                afterEach {
                    item = nil
                }
                
                it("adds item to store") {
                    expect(store.state().todos.items).to(equal([item]))
                }
                it("emits response") {
                    expect(fetchedItem).to(equal(item))
                }
            }
        }
    }
}
