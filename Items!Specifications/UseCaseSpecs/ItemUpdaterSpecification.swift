//
//  ItemUpdaterSpecification.swift
//  Items!Specifications
//
//  Created by vikingosegundo on 27.05.24.
//
import Quick
import Nimble
import ItemsApp
import ItemsModels

final class ItemUpdaterSpecification: QuickSpec {
    override class func spec() {
        var itemUpdater:ItemUpdater!
        var store:AppStore!
        var fetchedItem:TodoItem!
        var item:TodoItem!
        
        describe("ItemUpdater") {
            beforeEach {
                item = TodoItem(title: "Get Coffee")
                store = createDiskStore(pathInDocs: "ItemRemoverSpecifications.json")
                itemUpdater = ItemUpdater(store: store, responder: { response in
                    switch response {
                    case let .updated(t): fetchedItem = t
                    }
                })
                store.change(.add(item))
            }
            afterEach {
                destroy(&store)
                itemUpdater = nil
                fetchedItem = nil
                item = nil
            }
            context("store") {
                it("has one item") {
                    expect(store.state().todos.items).to(equal([item]))
                }
            }
            context("updating item") {
                var updatedItem:TodoItem!
                beforeEach {
                    updatedItem = item.alter(.title(to: "Get Coffee — ASAP"))
                    itemUpdater.request(to: .update(updatedItem))
                }
                
                it("has updated item in store") {
                    expect(store.state().todos.items).to(equal([updatedItem]))
                }
                it("emits response") {
                    expect(fetchedItem).to(equal(updatedItem))
                }
            }
        }
    }
}
