//
//  Message.swift
//  ItemsModels
//
//  Created by Manuel Meyer on 28.10.22.
//

public enum Message {
    case todos(Todos)
    public enum Todos {
        case cmd(CMD)
        case ack(ACK)
        public enum CMD {
            case add    (item:TodoItem)
            case remove (item:TodoItem)
            case update (item:TodoItem)
        }
        public enum ACK {
            case added  (item:TodoItem)
            case removed(item:TodoItem)
            case updated(item:TodoItem)
        }
    }
}
