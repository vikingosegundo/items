//
//  AppState.swift
//  ELPModels
//
//  Created by Manuel Meyer on 11.08.22.
//

public struct AppState {
    public enum Change {
        case add   (TodoItem)
        case remove(TodoItem)
        case update(TodoItem)
        case setting(Setting)
        public enum Setting {
            case todos(TodoList)
        }
    }
    public let todos:TodoList
    public init() { self.init(TodoList()) }
    public func alter(by changes:[Change]) -> Self { changes.reduce(self) { $0.alter(by:$1) } }
}
private extension AppState {
    init(_ t:TodoList) { todos = t }
    func alter(by change:Change) -> Self {
        switch change {
        case let .add   (t): return Self(todos.alter(.add   (.item(t))))
        case let .remove(t): return Self(todos.alter(.remove(.item(t))))
        case let .update(t): return Self(todos.alter(.update(.item(t))))
        case let .setting(.todos(todos)): return Self(todos)
        }
    }
}
extension AppState: Codable {}
