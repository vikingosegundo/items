//
//  Location.swift
//  ItemsModels
//
//  Created by vikingosegundo on 17/11/2022.
//

import Foundation
import Tools

//Domain Data types
public enum Location {
    public var id:Int { hashValue }
    case unknown
    case address   (Address   )
    case coordinate(Coordinate)
    case directions(Directions)
    public var address   :Address?    { switch self { case let .address   (a): a default: nil } }
    public var coordinate:Coordinate? { switch self { case let .coordinate(c): c default: nil } }
    public var directions:Directions? { switch self { case let .directions(d): d default: nil } }
}
public struct Address {
    public enum Change {
        case street (String)
        case city   (String)
        case country(String)
        case zipCode(String)
    }
    public  init(street:String, city:String, country:String, zipCode:String) { self.init(UUID(),street,city,country,zipCode) }
    private init(_ i:UUID,_ s:String,_ ci:String,_ co:String,_ z:String) { id=i; street=s; city=ci; country=co; zipCode=z }
    public let id     : UUID
    public let street : String
    public let city   : String
    public let country: String
    public let zipCode: String
    
    public  func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private func alter(_ c:Change) -> Self {
        switch c {
        case let .street (s):return .init(id,s,     city,country,zipCode)
        case let .city   (c):return .init(id,street,c,   country,zipCode)
        case let .country(c):return .init(id,street,city,c,      zipCode)
        case let .zipCode(z):return .init(id,street,city,country,z      )
        }
    }
}
public struct Coordinate {
    public init(latitude la:Double,longitude lo:Double)  { id = UUID(); latitude = la; longitude = lo }
    public let id       : UUID
    public let latitude : Double
    public let longitude: Double
}
public struct Directions {
    public enum Step:Identifiable, Hashable,Codable {
        public var id: Int { hashValue }
        public var text:String {
            switch self {
            case .step(let t): return t
            }
        }
        case step(String)
    }
    public enum Change {
        case replace(Replace); public enum Replace{
            case steps([Step])
        }
    }
    public init() { self.init(UUID(),[]) }
    public let id   : UUID
    public let steps: [Step]
    public func alter(_ c:Change) -> Self {
        switch c {
        case let .replace(.steps(s)): return .init(id,s)
        }
    }
}
private extension Directions {
    init(_ x:UUID,_ s:[Step]) { id = x; steps = s }
}

extension Location  : Codable,Hashable,Identifiable,Equatable {}
extension Address   : Codable,Hashable,Identifiable,Equatable {}
extension Coordinate: Codable,Hashable,Identifiable,Equatable {}
extension Directions: Codable,Hashable,Identifiable,Equatable {}
