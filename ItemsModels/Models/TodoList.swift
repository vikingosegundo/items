//
//  TodoList.swift
//  ItemsModels
//
//  Created by vikingosegundo on 17/11/2022.
//

import Foundation
import Tools

public struct TodoList: Identifiable {
    // members
    public let id   : UUID
    public let items: [TodoItem]
    
    public enum Change {
        case add   (Add   ); public enum Add    { case item(TodoItem) }
        case remove(Remove); public enum Remove { case item(TodoItem) }
        case update(Update); public enum Update { case item(TodoItem) }
    }
    
    // initializers
    public  init() { self.init(UUID(),[]) }
    private init(_ i:UUID,_ its:[TodoItem]) { id = i; items = its }
    
    public  func alter(_ c:Change...) -> Self { c.reduce(self) { $0.alter($1) } }
    private func alter(_ c:Change   ) -> Self {
        switch c {
        case let .add   (.item(i)): return Self(id,items + [i])
        case let .remove(.item(i)): return Self(id,items.filter{$0.id != i.id})
        case let .update(.item(i)): return
            items.contains(where:{ $0.id == i.id })
                ? self
                    .alter(
                        .remove(.item(i)),
                        .add   (.item(i)))
                : self
        }
    }
}
extension TodoList:Codable {}
