//
//  TodoDate.swift
//  ItemsModels
//
//  Created by Manuel Meyer on 28.10.22.
//

import Foundation
import Tools

public enum TodoDate {
    case unknown
    case date(Date)
    case timeSpan(TimeSpan)
    public var timeSpan:TimeSpan? { switch self { case let .timeSpan(t): return t default: return nil } }
    public var date:Date?         { switch self { case let .date    (d): return d default: return nil } }
}
public enum TimeSpan {
    public enum TimeComponent {
        public var id: Int { hashValue }
        case seconds
        case minutes
        case hours
        case days
    }
    public enum Start {
        public var id:Int { hashValue }
        case from(Date)
        public var date:Date { switch self { case let .from(d): return d } }
    }
    public enum End {
        public var id: Int { hashValue }
        case unknown
        case to(Date)
    }
    public var id:Int { hashValue }
    case start   (Start,End)
    case duration(Start,for:Int,TimeComponent)
    public var start:Date {
        switch self{
        case let .start   (s,_  ): return s.date
        case let .duration(s,_,_): return s.date
        }
    }
    public var end:Date? {
        switch self{
        case let .start(_,.to(end)): return end
        case     .start(_,.unknown): return nil
        case let .duration(s,a,c  ):
            let dc:DateComponents!
            switch c {
            case .seconds:dc = DateComponents(second:a)
            case .minutes:dc = DateComponents(minute:a)
            case .hours  :dc = DateComponents(hour  :a)
            case .days   :dc = DateComponents(day   :a)
            }
            return cal.date(byAdding:dc,to:s.date)
        }
    }
}
extension TodoDate              : Codable,Hashable,Equatable                 {}
extension TimeSpan              : Codable,Hashable,Identifiable,Equatable    {}
extension TimeSpan.Start        : Codable,Hashable,Identifiable,Equatable    {}
extension TimeSpan.End          : Codable,Hashable,Identifiable,Equatable    {}
extension TimeSpan.TimeComponent: Codable,Hashable,Identifiable,CaseIterable {}

fileprivate let cal = Calendar.autoupdatingCurrent
