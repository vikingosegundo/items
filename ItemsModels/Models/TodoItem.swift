//
//  TodoItem.swift
//  ItemsModels
//
//  Created by vikingosegundo on 17/11/2022.
//

import Foundation
import Tools

public struct TodoItem:Identifiable,Equatable {
    public enum Change:Codable,Equatable {
        case title   (to:String)
        case details (to:String)
        case due     (to:TodoDate)
        case location(to:Location)
        case   finish
        case unfinish
    }
    // members
    public let id      : UUID
    public let title   : String
    public let details : String
    public let state   : State
    public let due     : TodoDate
    public let location: Location
    public let created : Date
    public let changed : Date
    // initialisers
    public  init(title:String) {
        self.init(UUID(),title,"",.unfinished,.unknown,.unknown, .now, .now) }
    private init(_ i:UUID,_ t:String,_ ds:String,_ c:State,_ d:TodoDate,_ l:Location,_ cr:Date,_ cd:Date) { id=i; title=t; details=ds ;state=c; due=d; location=l; created=cr; changed=cd }
    
    public  func alter(_ c:Change...) -> Self { c.reduce(self) { return $0.alter($1) } }
    private func alter(_ c:Change   ) -> Self {
        switch c {
        case let    .title(to:t): return Self( id, t    ,details,       state, due, location,created,.now)
        case let  .details(to:d): return Self( id, title,d,             state, due, location,created,.now)
        case       .finish      : return Self( id, title,details,   .finished, due, location,created,.now)
        case     .unfinish      : return Self( id, title,details, .unfinished, due, location,created,.now)
        case let .location(to:l): return Self( id, title,details,       state, due, l       ,created,.now)
        case let .due(to:.timeSpan(.start(.from(s),.to(e)))):
                                  return e > s                            // check for timespans if dates are in correct order
            ? Self( id, title, details, state, .timeSpan(.start(.from(s),.to(e))), location,created, .now)
                                       : self
        case let .due(to:.timeSpan(.duration(.from(s),for:amount,comp))):   // check duration
                                  let e = cal.date(byAdding:dateComponent(comp,value:amount),to:s)
                                  return (e != nil && e! > s)                                     // is the end later than start?
                                       ? Self( id, title,details, state, .timeSpan(.duration(.from(s),for:amount,comp)), location,created,.now)
                                       : self
        case let .due(to:d)     : return Self( id, title,details, state, d, location, created,.now)
        }
    }
}

extension TodoItem {
    public enum State:Codable,Equatable {
        case unfinished
        case finished
    }
}
extension TodoItem:Codable {}
fileprivate let cal = Calendar.autoupdatingCurrent
func dateComponent(_ c:TimeSpan.TimeComponent, value v:Int) -> DateComponents {
    switch c {
    case .seconds: return .init(second: v)
    case .minutes: return .init(minute: v)
    case .hours  : return .init(hour  : v)
    case .days   : return .init(day   : v)
    }
}
