//
//  Item.HeaderView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI

extension ItemView {
    struct Header: View {
        @Binding           private var item    : TodoItemViewModel
        @EnvironmentObject private var todoList: TodoListViewModel
        @State             private var title   : String
        @State             private var details : String
        @FocusState private var isFocused:Bool
        init(item i: Binding<TodoItemViewModel>) { _item = i; title = i.wrappedValue.title; details = i.wrappedValue.details }
        
        var body: some View {
            VStack {
                TextField("Enter Title", text:$title)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .font(.system(size:18))
                TextField("Enter Details",text:$details,axis:.vertical)
                    .lineLimit(6...10)
                    .focused($isFocused)
                    .textFieldStyle(RoundedBorderTextFieldStyle())
                    .font(.system(size:18))
            }
            .padding()
            .onChange(of:title  ) { item.title = $0   }
            .onChange(of:details) { item.details = $0 }
            .onTapGesture {
                isFocused = false
            }
        }
    }
}
