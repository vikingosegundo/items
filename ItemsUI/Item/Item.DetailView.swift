//
//  Item.DetailView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI
import ItemsModels
import MapKit

extension ItemView {
    struct Details: View {
        @Binding       var item: TodoItemViewModel
        @State private var showingDatePicker = false
        
        var body: some View {
            VStack {
                Grid(alignment:.center,horizontalSpacing:20,verticalSpacing:20) { 
                    GridRow(alignment:.top) {
                        Text("due")
                            .gridColumnAlignment(.trailing)
                        VStack {
                            switch item.due {
                            case .unknown :Button { showDatePicker() } label: { ItemView.Due.UnknownView (item:$item) }
                            case .date    :Button { showDatePicker() } label: { ItemView.Due.DateView    (item:$item) }
                            case .timeSpan:Button { showDatePicker() } label: { ItemView.Due.TimeSpanDisplayView(item:$item) }
                            }
                        }
                        .foregroundStyle(actionColor)
                    }
                    GridRow(alignment:.top) {
                        Text("completed")
                            .gridColumnAlignment(.trailing)
                        Button {
                            item.completed.toggle()
                        } label: {
                            Image(systemName:item.completed
                                  ? "checkmark.square"
                                  : "square"
                            )
                        }
                        .foregroundStyle(actionColor)
                    }.onChange(of:item.completed) {
                        item.completed = $0
                    }
                }
                .font(.system(size:18))
                .onChange(of: item.due) { newValue in
                    showingDatePicker = false
                }
                
                ItemView.Loc.Selector(item: $item)
                
                switch item.location {
                case .unknown   :ItemView.Loc.UnknownView()
                case .address   :ItemView.Loc.AddressView   (for:$item)
                case .coordinate:ItemView.Loc.CoordinateView(for:$item)
                case .directions:ItemView.Loc.DirectionView (for:$item)
                }
            }
            .sheet(isPresented:$showingDatePicker) { ItemView.Due.EditView(item:$item).presentationDetents([.height(540), .medium]) }
        }
    }
}

private extension ItemView.Details {
    func showDatePicker() { showingDatePicker = true }
}
