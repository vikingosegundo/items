//
//  EditItem.swift
//  ItemsUI
//
//  Created by vikingosegundo on 25/11/2022.
//

import SwiftUI

extension ItemView {
    struct Edit:View {
        @Binding           var item    : TodoItemViewModel
        @EnvironmentObject var todoList: TodoListViewModel
        @State     private var confirmDeletion = false
        @FocusState private var isFocused:Bool
        var body: some View {
            VStack {
                ScrollView {
                    ItemView .Header(item:$item)
                    ItemView.Details(item:$item)
                    Spacer()
                }.onTapGesture {
                    isFocused = false
                }
            }
            
            .navigationTitle(item.title)
            .toolbar {
                Button(role:.destructive) {
                    confirmDeletion = true
                } label: { Text("delete") }
                .accentColor(actionColor)
            }
            .alert("Delete Item",
                   isPresented: $confirmDeletion,
                   actions: {
                Button(role:.destructive) { todoList.delete(item) } label: { Text("Delete Item") }
                Button(role:.cancel     ) {                       } label: { Text("Cancel"     ) } },
                   message: { Text("Please confirm deletion of \"\(item.title)\".") }
            )
        }
    }
}
