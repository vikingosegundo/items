//
//  Item.Loc.UnknownView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit

extension ItemView.Loc {
    struct UnknownView:View {
        var body: some View {
            Text("")
        }
    }
}
