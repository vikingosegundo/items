//
//  Item.Loc.DirectionView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit

extension ItemView.Loc {
    struct DirectionView:View {
        @Binding private var item:TodoItemViewModel
        
        init(for i:Binding<TodoItemViewModel>) {
            guard
                let _ = i.location.wrappedValue.directions
            else { fatalError("location must be Directions") }
            _item = i
        }

        var body: some View {
            HStack {
                List {
                    ForEach(item.location.directions!.steps,id:\.id) {
                        Text("\($0.text)")
                    }
                }
            }
        }
    }
}
