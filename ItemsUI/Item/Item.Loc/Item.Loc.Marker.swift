//
//  Item.Loc.Marker.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit
import ItemsModels

extension ItemView.Loc {
    struct Marker: Identifiable {
        var id: String {
            "\(coordinate.id)-\(title.hashValue)"
        }
        let coordinate: Coordinate
        let title     : String
    }
}
