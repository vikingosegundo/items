//
//  Item.Loc.Selector.swift
//  ItemsUI
//
//  Created by vikingosegundo on 31/01/2023.
//

import SwiftUI
import ItemsModels

extension ItemView.Loc {
    struct Selector:View {
        
        enum InputMode: String, Identifiable, Equatable {
            case unknown
            case address
            case coordinates
            case directions
            
            var id: String {rawValue}
        }

        @Binding private var item     : TodoItemViewModel
        @State   private var inputMode: InputMode?
        
        init(item i:Binding<TodoItemViewModel>) {_item = i}
        
        var body: some View {
            Grid {
                GridRow {
                    Text("unknown")
                    Text("address")
                    Text("map")
                    Text("directions")
                }
                GridRow {
                    Button { inputMode = .unknown     } label: { imgForInputMode(.unknown    ) }
                    Button { inputMode = .address     } label: { imgForInputMode(.address    ) }
                    Button { inputMode = .coordinates } label: { imgForInputMode(.coordinates) }
                    Button { inputMode = .directions  } label: { imgForInputMode(.directions ) }
                }
            }
            .padding(.top)
            .alert(item: $inputMode) { input in
               return Alert(
                    title: Text("Do you really want to change the loction?"),
                    message: Text("Switching location will reset the current value"),
                    primaryButton: .default(Text("Yes"), action: {handleInputMode(input)}),
                    secondaryButton: .cancel()
               )
            }
        }
        
        private func handleInputMode(_ mode: InputMode) {
            switch mode {
            case .unknown    : item.location = .unknown
            case .address    : item.location = .address(.init(street: "", city: "", country: "", zipCode: ""))
            case .coordinates: item.location = .coordinate(.init(latitude:53.217222, longitude:6.574722))
            case .directions : item.location = .directions(.init())
            }
        }
        
        private func imgForInputMode(_ m: InputMode) -> Image {
            Image(systemName:item.location.mapToInputMode() == m ? "checkmark.circle" : "circle")
            
        }
    }
}

extension Location {
    func mapToInputMode() -> ItemView.Loc.Selector.InputMode {
        switch self {
        case .unknown: return .unknown
        case .address: return .address
        case .coordinate: return .coordinates
        case .directions: return .directions
        }
    }
}
