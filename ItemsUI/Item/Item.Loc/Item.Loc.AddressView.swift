//
//  Item.Loc.AddressView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit
import ItemsModels

extension ItemView.Loc {
    struct AddressView:View {
        @Binding         var item   : TodoItemViewModel
        @State   private var street : String
        @State   private var city   : String
        @State   private var country: String
        @State   private var zip    : String

        init(for i:Binding<TodoItemViewModel>) {
            guard
                let a = i.location.wrappedValue.address
            else { fatalError("location must be Address") }
            _item    = i
            _street  = State(initialValue:a.street )
            _city    = State(initialValue:a.city   )
            _country = State(initialValue:a.country)
            _zip     = State(initialValue:a.zipCode)
        }
        var body: some View {
            VStack {
                Grid(alignment: .center, verticalSpacing:8) {
                    GridRow { Text("street:").gridColumnAlignment(.trailing)
                        TextField("street", text:$street)
                    }
                    GridRow { Text("city:").gridColumnAlignment(.trailing)
                        HStack {
                            TextField("zip",  text:$zip).frame(width:65)
                            TextField("city", text:$city)
                        }
                    }
                    GridRow { Text("country:").gridColumnAlignment(.trailing)
                        TextField("country", text:$country)
                    }
                }
            }
            .padding()
            .onChange(of:street ) { _ in updateAddress() }
            .onChange(of:city   ) { _ in updateAddress() }
            .onChange(of:country) { _ in updateAddress() }
            .onChange(of:zip    ) { _ in updateAddress() }
        }
        private func updateAddress() {
            item.location = .address(
                Address(
                     street:street ,
                       city:city   ,
                    country:country,
                    zipCode:zip    )
            )
        }
    }
}
