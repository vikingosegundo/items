//
//  Item.Loc.CoordinateView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit
import ItemsModels

extension ItemView.Loc {
    struct CoordinateView:View {
        @Binding       var item  : TodoItemViewModel
        @State private var region: MKCoordinateRegion
        @State private var editing = false
        
        private var centerCoordinate: Coordinate {
            Coordinate(
                latitude: region.center.latitude,
                longitude: region.center.longitude
            )
        }
        
        init(for i:Binding<TodoItemViewModel>) {
            guard
                let coord = i.location.wrappedValue.coordinate
            else { fatalError("location has to be a Coordinate")}
            _item = i
            _region = .init(initialValue:.init(
                center:.init(latitude:coord.latitude,
                            longitude:coord.longitude),
                  span:.init(latitudeDelta:0.65,
                            longitudeDelta:0.65)
            ))
        }
        
        var body: some View {
            VStack {
                button
                Map(coordinateRegion: $region,
                    annotationItems: [ItemView.Loc.Marker(coordinate:item.location.coordinate!,title:item.title)]
                ) {
                    MapAnnotation(coordinate:locationCoordinateFrom($0.coordinate)) {
                        ItemView.Loc.AnnotationView(item: item).opacity(editing ? 0 : 1)
                    }
                }
                .overlay(pin)
                .frame(width: 300, height: 300)
            }
        }
        
        var   editButton: some View {Button("Edit")   { editing = true  }}
        var   saveButton: some View {Button("Save")   { saveAction()    }}
        var cancelButton: some View {Button("Cancel") { editing = false }}
        
        
        var pin: some View {
            Image(systemName: "mappin.circle.fill")
                .background(Color.white.cornerRadius(99))
                .opacity(editing ? 1 : 0)
        }
        
        @ViewBuilder var button: some View {
            HStack {
                Button("Cancel") { editing = false }.display(if: editing)
                Spacer()
                if editing {saveButton}
                else {editButton}
            }
            .frame(width: 300)
            .padding(.top, 24)
        }
        
        func saveAction() {
            item.location = .coordinate(centerCoordinate)
            editing = false
        }
    }
}

extension View {
    @ViewBuilder
    func display(if condition: Bool) -> some View {
        if condition {self}
        else {EmptyView()}
    }
}
fileprivate func locationCoordinateFrom(_ c:Coordinate) -> CLLocationCoordinate2D { .init(latitude:c.latitude,longitude:c.longitude) }
