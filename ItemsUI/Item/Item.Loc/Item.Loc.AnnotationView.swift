//
//  Item.Loc.AnnotationView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI
import MapKit

extension ItemView.Loc {
    struct AnnotationView: View {
        let item: TodoItemViewModel
        
        var body: some View {
            VStack(spacing: 0) {
                Text(item.title)
                    .font(Font.caption)
                    .padding(5)
                    .background(Color(.white))
                    .cornerRadius(10)
            }.offset(.init(width:0, height:5))
        }
    }
}
