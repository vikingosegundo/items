//
//  Item.Due.TimeSpanInputView.DurationInputView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 21/12/2022.
//

import SwiftUI
import ItemsModels

extension ItemView.Due.TimeSpanInput {
    struct Duration:View {
        @Binding  var startDate   : Date
        @Binding  var duration    : Int
        @Binding  var durationUnit: TimeSpan.TimeComponent
        @Binding  var mode        : TimeSpanKind

        var body: some View {
            VStack {
                Grid {
                    GridRow {
                        Text("from:")
                        DatePicker("", selection: $startDate)
                            .foregroundStyle(gradient)
                    }
                    GridRow {
                        Button {
                            switch mode {
                            case .startTo: mode = .duration
                            case .duration: mode = .startTo
                            }
                        } label: {
                            Text("for:")
                                .underline()
                                .foregroundColor(actionColor)
                        }
                        HStack {
                            TextField("number", text: Binding(
                                get: { String(duration) },
                                set: { duration = max(Int($0) ?? 0,0) }
                            ))
                            .keyboardType(.numberPad)
                            .frame(width:40)
                            .multilineTextAlignment(.center)
                            .overlay(RoundedRectangle(cornerRadius:2)
                                .stroke(gradient,lineWidth:1)
                            )
                            Picker(selection: $durationUnit) {
                                ForEach(TimeSpan.TimeComponent.allCases, id:\.self) {
                                    Text(String(describing:$0).lowercased())
                                }
                            } label: { Text("duration unit") }
                                .tint(gradient)
                        }
                    }
                }
                .frame(width: 300)
            }
        }
    }
}
