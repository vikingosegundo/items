//
//  Item.Due.TimeSpanView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI
import ItemsModels

extension ItemView.Due {
    struct TimeSpanDisplayView:View {
        
        @Binding var item    : TodoItemViewModel

        var body: some View {
            Grid {
                GridRow {
                    switch item.due.timeSpan {
                    case let .start(.from(f),.unknown):
                        Grid {
                            row("start:","\(df.string(from:f))")
                        }
                    case let .start(.from(f),.to(e)):
                        Grid {
                            row("from:","\(df.string(from:f))")
                            row("to:",  "\(df.string(from:e))")
                        }
                    case let .duration(.from(f),for:d,x):
                        Grid {
                            row("from:","\(df.string(from:f))"       )
                            row("for:", "\(stringFor(amount:d,in:x))")
                        }
                    case .none:
                        Text("unknown")
                    }
                }
            }
            .foregroundColor(actionColor)
        }
    }
}
fileprivate let df = createDateFormater()
fileprivate func createDateFormater() -> DateFormatter {
    let df = DateFormatter()
    df.timeStyle = .short
    df.dateStyle = .short
    return df
}
@ViewBuilder
fileprivate func row(_ key:String,_ defintion:String) -> some View {
    GridRow {
        Text(key)
            .frame(width:50,alignment:.trailing)
            .gridColumnAlignment(.trailing)
        Text(defintion)
    }
}
fileprivate func stringFor(amount:Int, in timeComponent:TimeSpan.TimeComponent) -> String {
    switch timeComponent {
    case .seconds: return "\(amount) \(amount != 1 ? "seconds" : "second")"
    case .minutes: return "\(amount) \(amount != 1 ? "minutes" : "minute")"
    case .hours  : return "\(amount) \(amount != 1 ? "hours"   : "hour"  )"
    case .days   : return "\(amount) \(amount != 1 ? "days"    : "day"   )"
    }
}
