//
//  Item.Due.DateInput.swift
//  ItemsUI
//
//  Created by vikingosegundo on 22/12/2022.
//

import SwiftUI

extension ItemView.Due {
    struct DateInput:View {
        @Binding var singleDate: Date

        var body: some View {
            VStack {
                DatePicker(selection: $singleDate,
                 displayedComponents: [.date,.hourAndMinute],
                               label: { Text("Date:") })
            }.padding()
        }
    }
}
