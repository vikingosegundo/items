//
//  Item.Due.TimeSpanInputView.FromToInputView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 21/12/2022.
//

import SwiftUI

extension ItemView.Due.TimeSpanInput {
    struct FromTo:View {
        @Binding var startDate: Date
        @Binding var endDate  : Date
        @Binding var mode     : TimeSpanKind
        
        var body: some View {
            VStack {
                Grid {
                    GridRow {
                        Text("from:")
                        DatePicker("",selection:$startDate)
                            .foregroundStyle(gradient)
                    }
                    GridRow {
                        Button {
                            switch mode {
                            case .startTo : mode = .duration
                            case .duration: mode = .startTo
                            }
                        } label: {
                            Text("to:")
                                .underline()
                                .foregroundColor(actionColor)
                        }
                        DatePicker("",selection:$endDate,in:startDate...)
                            .foregroundStyle(gradient)
                    }
                }
                .frame(width: 300)
            }
        }
    }
}
