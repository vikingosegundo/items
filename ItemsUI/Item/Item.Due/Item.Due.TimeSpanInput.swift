//
//  Item.Due.TimeSpanInputView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 21/12/2022.
//

import SwiftUI
import ItemsModels

extension ItemView.Due {
    struct TimeSpanInput:View {
        enum TimeSpanKind { case startTo, duration }
        init(timeSpan ts:Binding<TimeSpan>) {
            _timeSpan = ts
            switch timeSpan {
            case .start   : _mode = State(initialValue:.startTo )
            case .duration: _mode = State(initialValue:.duration)
            }
            switch timeSpan {
            case let .start(.from(s), .to(e)    ): _startDate    = State(initialValue:s);_endDate  = State(initialValue:e)
            case let .start(.from(s),.unknown   ): _startDate    = State(initialValue:s);_endDate  = State(initialValue:.distantPast)
            case let .duration(.from(s), for:d,u): _durationUnit = State(initialValue:u);_duration = State(initialValue:d);_startDate = State(initialValue:s);_endDate = State(initialValue:.distantPast)
            }
        }
        @Binding         var timeSpan    : TimeSpan
        @State   private var mode        : TimeSpanKind           = .startTo
        @State   private var duration    : Int                    = 1
        @State   private var durationUnit: TimeSpan.TimeComponent = .days
        @State   private var startDate   : Date                   = .distantPast
        @State   private var endDate     : Date                   = .distantPast
        
        var body: some View {
            VStack {
                switch mode {
                case .startTo : FromTo  (startDate:$startDate,endDate:$endDate,mode:$mode)
                case .duration: Duration(startDate:$startDate,duration:$duration,durationUnit:$durationUnit,mode:$mode)
                }
            }
            .onChange(of:mode) {
                switch $0 {
                case .startTo : timeSpan = .start(.from(startDate), .to(endDate))
                case .duration: timeSpan = .duration(.from(startDate), for:duration,durationUnit)
                }
            }
            .onChange(of:startDate) {
                switch mode {
                case .startTo : timeSpan = .start(.from($0), .to(endDate > startDate ? endDate : startDate))
                case .duration: timeSpan = .duration(.from($0),for:duration,durationUnit)
                }
            }
            .onChange(of:endDate     ) { timeSpan = .start(.from(startDate), .to($0 > startDate ? $0 : startDate)) }
            .onChange(of:duration    ) { timeSpan = .duration(.from(startDate),for:$0,durationUnit               ) }
            .onChange(of:durationUnit) { timeSpan = .duration(.from(startDate),for:duration,$0                   ) }
        }
    }
}
