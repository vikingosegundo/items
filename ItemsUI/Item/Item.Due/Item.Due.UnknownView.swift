//
//  Item.Due.UnknownView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI

extension ItemView.Due {
    struct UnknownView:View {
        @Binding var item: TodoItemViewModel
        
        var body: some View {
            Text("unknown")
        }
    }
}
