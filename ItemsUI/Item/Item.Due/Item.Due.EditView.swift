//
//  Item.Due.EditView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI
import ItemsModels

fileprivate let df = createDateFormater()
fileprivate let defaultSingleDate: Date = .tomorrow.noon
fileprivate let defaultTimeSpan: TimeSpan = .start(.from(defaultSingleDate), .to(defaultSingleDate.dayAfter.noon))

extension ItemView.Due {
    struct EditView:View {
        enum InputMode {
            case unknown
            case date
            case timeSpan
        }

        @Binding         var item        : TodoItemViewModel
        @State   private var dueDate     : TodoDate
        @State   private var intermediate: TodoDate
        @State   private var inputMode   : InputMode
        @State   private var singleDate  : Date
        @State   private var timeSpan    : TimeSpan
    
        init(item i:Binding<TodoItemViewModel>) {
            _item         = i
            _timeSpan     = State(initialValue:  extractTimeSpan(i.wrappedValue.due) ?? defaultTimeSpan  )
            _singleDate   = State(initialValue:extractSingleDate(i.wrappedValue.due) ?? defaultSingleDate)
            _dueDate      = State(initialValue:i.wrappedValue.due)
            _intermediate = State(initialValue:i.wrappedValue.due)

            switch i.wrappedValue.due {
            case .unknown : _inputMode = State(initialValue:.unknown )
            case .date    : _inputMode = State(initialValue:.date    )
            case .timeSpan: _inputMode = State(initialValue:.timeSpan)
            }
        }
        public var body: some View {
            VStack {
                Text("Set due date for")
                    .padding(.bottom, 8)
                Text("\(item.title)")
                    .font(.title)
                    .padding(.bottom, 24)
                Grid {
                    GridRow { Button { inputMode = .unknown } label: { imgForInputMode(.unknown ) }.foregroundColor(actionColor); viewForUnknown()               }.padding(.bottom)
                    GridRow { Button { inputMode = .date    } label: { imgForInputMode(.date    ) }.foregroundColor(actionColor); viewFor(date:dateFromDates()!) }.padding(.bottom)
                    GridRow { Button { inputMode = .timeSpan} label: { imgForInputMode(.timeSpan) }.foregroundColor(actionColor); viewFor(span:spanFromDates()!) }.padding(.bottom)
                }
                .padding()
                VStack {
                    switch inputMode {
                    case .unknown : EmptyView()
                    case .date    : VStack {ItemView.Due.DateInput(singleDate:$singleDate) }
                    case .timeSpan: ItemView.Due.TimeSpanInput(timeSpan:$timeSpan)
                    }
                    Button {dueDate = intermediate} label: { Text("Set Due Date") }
                }
                .frame(minHeight:100)
                .padding(.bottom)
                
            }.onChange(of:inputMode) {
                switch $0 {
                case .unknown : intermediate = .unknown
                case .timeSpan: intermediate = .timeSpan(timeSpan)
                case .date    : intermediate = .date(singleDate)
                }
            }.onChange(of:singleDate) {
                intermediate = .date($0)
            }.onChange(of:timeSpan) {
                intermediate = .timeSpan($0)
            }.onChange(of:dueDate) {
                item.due = $0
            }
        }
        private func dateFromDates() -> Date?     { switch dueDate{ case .date    (let d): return d default: return defaultSingleDate } }
        private func spanFromDates() -> TimeSpan? { switch dueDate{ case .timeSpan(let s): return s default: return defaultTimeSpan   } }
        private func imgForInputMode(_ m:ItemView.Due.EditView.InputMode) -> Image { Image(systemName:inputMode == m ? "checkmark.circle" : "circle") }
    }
}

// MARK: - displaying view builder
extension ItemView.Due.EditView {
    @ViewBuilder fileprivate func viewForUnknown(      ) -> some View { Text("unknown"  ) }
    @ViewBuilder fileprivate func viewFor(date:Date    ) -> some View { Text("Date"     ) }
    @ViewBuilder fileprivate func viewFor(span:TimeSpan) -> some View { Text("Time Span") }
}
// MARK: - helper & tools
fileprivate func createDateFormater() -> DateFormatter {
    let df = DateFormatter()
    df.timeStyle = .short
    df.dateStyle = .short
    return df
}
fileprivate func extractSingleDate(_ d:TodoDate) -> Date? {
    switch d {
    case .date(let d): return d
    default: return nil
    }
}
fileprivate func extractTimeSpan(_ t:TodoDate) -> TimeSpan? {
    switch t {
    case .timeSpan(let t): return t
    default: return nil
    }
}
fileprivate func stringFor(amount:Int, in timeComponent:TimeSpan.TimeComponent) -> String {
    switch timeComponent {
    case .seconds : return "\(amount) \(amount != 1 ? "seconds" : "second")"
    case .minutes : return "\(amount) \(amount != 1 ? "minutes" : "minute")"
    case .hours   : return "\(amount) \(amount != 1 ? "hours"   : "hour"  )"
    case .days    : return "\(amount) \(amount != 1 ? "days"    : "day"   )"
    }
}
