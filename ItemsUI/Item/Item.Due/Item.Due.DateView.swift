//
//  Item.Due.DateView.swift
//  ItemsUI
//
//  Created by vikingosegundo on 13/12/2022.
//

import SwiftUI

fileprivate let df = createDateFormater()

extension ItemView.Due {
    struct DateView:View {
        @Binding var item: TodoItemViewModel
        
        var body: some View {
            Text("\(item.due.date != nil ? df.string(from:item.due.date!) : "")")
        }
    }
}
fileprivate func createDateFormater() -> DateFormatter {
    let df = DateFormatter()
    df.timeStyle = .short
    df.dateStyle = .short
    return df
}
