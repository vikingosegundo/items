//
//  ItemRow.swift
//  ItemsUI
//
//  Created by vikingosegundo on 25/11/2022.
//

import SwiftUI

extension ItemView {
    struct Row: View {
        @Binding           var item            : TodoItemViewModel
        @EnvironmentObject var todoList        : TodoListViewModel
        @State     private var showDateSelector: Bool = false
        
        var body: some View {
            VStack {
                HStack {
                    Image(systemName:item.completed
                          ? "checkmark.square"
                          : "square"
                    )
                    .onTapGesture { item.completed.toggle() }
                    Text(item.title)
                }.contextMenu {
                    Button                    { item.completed.toggle() } label: { Text(item.completed ? "unfinish" : "finish") }
                    Button                    { showDateSelector = true } label: { Text("set due date"                        ) }
                    Button(role:.destructive) { todoList.delete(item)   } label: { Text("delete"                              ) }
                }
            }.sheet(isPresented:$showDateSelector) {
                ItemView.Due.EditView(item:$item)
            }
        }
    }
}
