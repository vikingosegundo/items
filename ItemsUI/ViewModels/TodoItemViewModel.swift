//
//  TodoItemViewModel.swift
//  ItemsUI
//
//  Created by vikingosegundo on 25/11/2022.
//

import ItemsModels
import SwiftUI

public final class TodoItemViewModel: Identifiable, ObservableObject, Equatable {
    init(for i:TodoItem, update u:@escaping (TodoItemViewModel) -> ()) {
        backedItem  = i
        update      = u
        id          = i.id
        title       = i.title
        details     = i.details
        due         = i.due
        location    = i.location
        created     = i.created
        changed     = i.changed
        completed   = i.state == .finished
    }
    public  var item       : TodoItem { backedItem }
    private var backedItem : TodoItem { didSet { update(self) } }
    private var update     : (TodoItemViewModel) ->()
    @Published public var id       : UUID
    @Published public var created  : Date
    @Published public var changed  : Date
    @Published public var title    : String   { didSet { backedItem = backedItem.alter(.title(to:title)               ) } }
    @Published public var details  : String   { didSet { backedItem = backedItem.alter(.details(to:details)           ) } }
    @Published public var completed: Bool     { didSet { backedItem = backedItem.alter(completed ? .finish : .unfinish) } }
    @Published public var due      : TodoDate { didSet { backedItem = backedItem.alter(.due(to:due)                   ) } }
    @Published public var location : Location { didSet { backedItem = backedItem.alter(.location(to:location)         ) } }
}
extension TodoItemViewModel {
    public static func == (lhs:TodoItemViewModel, rhs:TodoItemViewModel) -> Bool { rhs.id == lhs.id }
}
