//
//  TodoListViewModel.swift
//  ELPUI
//
//  Created by Manuel Meyer on 19.08.22.
//

import ItemsModels
import SwiftUI

public final class TodoListViewModel:ObservableObject {
    @Published public var items: [TodoItemViewModel] = []
    public init(store:Store<AppState,AppState.Change>, roothandler r: @escaping(Message) -> ()) {
        roothandler = r
        store.updated { self.process(store.state()) }
        process(store.state())
    }
    public func add   (_ i:TodoItem         ) { roothandler(.todos(.cmd(.add   (item:i     )))) }
    public func delete(_ i:TodoItemViewModel) { roothandler(.todos(.cmd(.remove(item:i.item)))) }
    public func update(_ i:TodoItemViewModel) { roothandler(.todos(.cmd(.update(item:i.item)))) }
    
    public func process(_ appState:AppState) {
        DispatchQueue.main.async {
            self.items = appState.todos.items.map{TodoItemViewModel(for:$0, update:self.update) }
        }
    }
    private let roothandler:(Message) -> ()
}
