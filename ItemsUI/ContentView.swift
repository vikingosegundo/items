//
//  ContentView.swift
//  Items!
//
//  Created by Manuel Meyer on 28.10.22.
//

import ItemsModels
import SwiftUI
import Tools

public let gradient = LinearGradient(
    colors: [.pink, .orange],
    startPoint: .top,
    endPoint: .bottom
)

public let actionColor = Color.red

public struct ContentView: View {
    @StateObject private var todoList:TodoListViewModel
    @State       private var presentTitleInput = false
    @State       private var enteredTitle = ""
    
    public init(todoList tl: TodoListViewModel) { _todoList = StateObject(wrappedValue:tl) }
    public var body: some View {
        VStack {
            NavigationView {
                List {
                    Section("todos".uppercased()) {
                        if todoList.items.count > 0 {
                            if !todos.isEmpty {
                                ForEach(todos, id:\.id) { i in
                                    NavigationLink(destination:ItemView.Edit(item:i)) { ItemView.Row(item:i) }
                                }.onDelete {
                                    $0.forEach {
                                        todoList.delete(todos[$0].wrappedValue)
                                    }
                                }
                            }
                        } else {
                            Text("no items found")
                        }
                    }
                }.toolbar { Button { askTitleForNewItem() } label: { Image(systemName:"plus") } }

            }
            .animation(.linear, value: todoList.items)
            .navigationTitle("Todos")
            .accentColor(actionColor)
        }
        .environmentObject(todoList)
        .foregroundStyle(gradient)
        .alert("New Item",
               isPresented: $presentTitleInput,
               actions: {
            TextField("title", text:$enteredTitle)
            Button               {   save() } label: { Text("Add Item") }
            Button(role:.cancel) { cancel() } label: { Text("Cancel"  ) } },
               message: { Text("Please enter title for new Item.") }
        )
        .onAppear {
            UINavigationBar.appearance().titleTextAttributes      = [NSAttributedString.Key.foregroundColor: UIColor.orange]
            UINavigationBar.appearance().largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.orange]
        }
        
    }
    private func bind(_ item: TodoItemViewModel) -> Binding<TodoItemViewModel> {
        Binding(
            get: { item },
            set: { todoList.items[todoList.items.firstIndex(of:item)!] = $0 }
        )
    }
    private var todos: [Binding<TodoItemViewModel> ] { todoList.items.sorted{ i0, i1 in
          (i0.completed ? 1 : 0, i1.changed)
        < (i1.completed ? 1 : 0, i0.changed)
    }.map { bind($0)} }
}

private extension ContentView {
    func               save() { saveToList()             }
    func askTitleForNewItem() { presentTitleInput = true }
    func             cancel() { reset()                  }
    func              reset() { enteredTitle = ""        }
}

private extension ContentView {
    func saveToList() {
        !enteredTitle.isEmpty()
            ? todoList.add(TodoItem(title:enteredTitle.trimmed()))
            : ()
        reset()
    }
}
