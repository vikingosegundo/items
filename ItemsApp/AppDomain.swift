//
//  AppDomain.swift
//  ELPMainApp
//
//  Created by Manuel Meyer on 19.08.22.
//

import ItemsModels

public typealias  Input = (Message) -> ()
public typealias Output = (Message) -> ()

public func createAppDomain(
    store      : Store<AppState,AppState.Change>,
    receivers  : [Input],
    rootHandler: @escaping Output
) -> Input
{
    let features: [Input] = [
       createTodoListFeature(store:store,output:rootHandler)
    ]
    
    return { msg in
        (receivers + features).forEach {
            $0(msg)
        }
    }
}
