//
//  ItemAdder.swift
//  ItemsApp
//
//  Created by Manuel Meyer on 29.10.22.
//

import ItemsModels

public struct ItemAdder:UseCase {
    public enum Request { case add  (TodoItem) }
    public enum Response{ case added(TodoItem) }
    
    public init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        interactor = Interactor(store: s, responder: r)
    }

    private let interactor:Interactor
    
    public func request(to request: Request) {
        switch request {
        case let .add(t): interactor.add(item: t)
        }
    }
    
    typealias RequestType = Request
    typealias ResponseType = Response
}
private extension ItemAdder {
    private struct Interactor {
        init(store: Store<AppState,AppState.Change>, responder: @escaping (Response) -> ()) {
            self.store   = store
            self.respond = responder
        }
        
        func add(item i: TodoItem) {
            store.change(.add(i))
            respond(.added(i))
        }
        
        private let store: Store<AppState,AppState.Change>
        private let respond: (Response) -> ()
    }
}
