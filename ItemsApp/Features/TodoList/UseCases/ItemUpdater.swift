//
//  ItemUpdater.swift
//  ItemsApp
//
//  Created by Manuel Meyer on 30.10.22.
//

import ItemsModels

public struct ItemUpdater:UseCase {
    public enum Request { case update (TodoItem) }
    public enum Response{ case updated(TodoItem) }
    public init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        interactor = Interactor(store:s, responder:r)
    }

    private let interactor:Interactor
    public func request(to request: Request) {
        switch request {
        case let .update(t): interactor.update(item:t)
        }
        
    }
    typealias RequestType = Request
    typealias ResponseType = Response
}

private extension ItemUpdater {
    private struct Interactor {
        init(store: Store<AppState,AppState.Change>, responder: @escaping (Response) -> ()) {
            self.store   = store
            self.respond = responder
        }
        
        func update(item i: TodoItem) {
            store.change(.update(i))
            respond(.updated(i))
        }
        
        private let store: Store<AppState,AppState.Change>
        private let respond: (Response) -> ()
    }
}
