//
//  ItemRemover.swift
//  ItemsApp
//
//  Created by Manuel Meyer on 29.10.22.
//

import ItemsModels

public struct ItemRemover:UseCase {
    public enum Request { case remove (TodoItem) }
    public enum Response{ case removed(TodoItem) }
   
    typealias RequestType = Request
    typealias ResponseType = Response
    
    public init(store s:Store<AppState,AppState.Change>, responder r: @escaping (Response) -> ()) {
        interactor = Interactor(store:s, responder:r)
    }
    private let interactor:Interactor
    
    public func request(to request:Request) {
        switch request {
        case let .remove(t):interactor.remove(item:t)
        }
    }
}

private extension ItemRemover {
    private struct Interactor {
        init(store: Store<AppState,AppState.Change>, responder: @escaping (Response) -> ()) {
            self.store   = store
            self.respond = responder
        }
        
        func remove(item i: TodoItem) {
            store.change(.remove(i))
            respond(.removed(i))
        }
        
        private let store: Store<AppState,AppState.Change>
        private let respond: (Response) -> ()
    }
}
