//
//  createTodoListFeature.swift
//  ItemsApp
//
//  Created by Manuel Meyer on 28.10.22.
//

import ItemsModels

public func createTodoListFeature(
    store    s: Store<AppState,AppState.Change>,
    output out: @escaping Output
) -> Input
{
    let adder   = ItemAdder  (store:s,responder:process(on:out))
    let remover = ItemRemover(store:s,responder:process(on:out))
    let updater = ItemUpdater(store:s,responder:process(on:out))
    func execute(cmd:Message.Todos.CMD) {
        switch cmd {
        case let .add   (item:t): adder  .request(to:.add   (t) )
        case let .remove(item:t): remover.request(to:.remove(t) )
        case let .update(item:t): updater.request(to:.update(t) )
        }
    }
    return {
        if case let .todos(.cmd(c)) = $0 { execute(cmd:c) }
    }
}
// MARK - Processing
private func process(on out:@escaping Output) -> (ItemAdder  .Response) -> () {
    { 
        switch $0 { 
        case let .added(t):out(.todos(.ack(.added(item:t))))
        }
    }
}
private func process(on out:@escaping Output) -> (ItemRemover.Response) -> () {
    {
        switch $0 {
        case let .removed(t):out(.todos(.ack(.removed(item:t))))
        }
    }
}
private func process(on out:@escaping Output) -> (ItemUpdater.Response) -> () {
    {
        switch $0 {
            case let .updated(t):out(.todos(.ack(.updated(item:t))))
        }
    }
}
