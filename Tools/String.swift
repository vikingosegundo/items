//
//  String.swift
//  Tools
//
//  Created by vikingosegundo on 10/12/2022.
//

public extension String {
    func trimmed() -> String { trimmingCharacters(in:.whitespacesAndNewlines) }
    func isEmpty() -> Bool   { trimmed() == ""                                }
}
