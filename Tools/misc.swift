//
//  misc.swift
//  Tools
//
//  Created by Manuel Meyer on 30.10.22.
//

public func -<T: RangeReplaceableCollection>(lhs:T,rhs:T) -> T where T.Iterator.Element:Identifiable { var lhs = lhs; rhs.forEach { el in if let idx = lhs.firstIndex(where:{$0.id == el.id}) { lhs.remove(at:idx) } }; return lhs }
//public func -<T: RangeReplaceableCollection>(lhs:T,rhs:T) -> T where T.Iterator.Element:Equatable    { var lhs = lhs; rhs.forEach { el in if let idx = lhs.firstIndex(of:el                 ) { lhs.remove(at:idx) } }; return lhs }
public func ==(lhs:String.SubSequence,rhs:String) -> Bool { String(lhs) == rhs }
