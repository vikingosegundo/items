//
//  Dates.swift
//  Tools
//
//  Created by Manuel Meyer on 30.10.22.
//

public extension Date {
    static var     today: Date { Date().midnight      }
    static var yesterday: Date { Date.today.dayBefore }
    static var  tomorrow: Date { Date.today.dayAfter  }
    
    var dayBefore: Date { cal.date(byAdding:.day,value:-1,to:self)! }
    var  dayAfter: Date { cal.date(byAdding:.day,value: 1,to:self)! }
    var  midnight: Date { at(00,00)! }
    var      noon: Date { at(12,00)! }

    func at(_ h:Int,_ m:Int,_ s:Int = 0) -> Date? { cal.date(bySettingHour:h,minute:m,second:s,of:self) }
}

fileprivate extension Date {
    var cal: Calendar { Calendar.autoupdatingCurrent }
}
