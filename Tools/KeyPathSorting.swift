//
//  KeyPathSorting.swift
//  Tools
//
//  Created by Manuel Meyer on 30.10.22.
//

extension Collection {
    public func sorted<Value: Comparable>(on property: KeyPath<Element, Value>, by areInIncreasingOrder: (Value, Value) -> Bool) -> [Element] {
        sorted { currentElement, nextElement in
            areInIncreasingOrder(currentElement[keyPath: property], nextElement[keyPath: property])
        }
    }
}
