//
//  Items_App.swift
//  Items!
//
//  Created by Manuel Meyer on 28.10.22.
//

import ItemsModels
import SwiftUI
import ItemsUI
import ItemsApp
@main
struct Items_App: App {
    var body: some Scene {
        WindowGroup {
            ContentView(todoList:todoListVM)
        }
    }
}
fileprivate let store      : Store              = createDiskStore()
fileprivate let todoListVM : TodoListViewModel  = TodoListViewModel(store:store, roothandler:{ rootHandler($0) })
fileprivate let rootHandler: ((Message) -> ())! = createAppDomain(
    store      : store,
    receivers  : [],
    rootHandler: { rootHandler($0) }
)
fileprivate func prt(m:Message) {
    print(m)
}
